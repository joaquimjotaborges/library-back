Rails.application.config.content_security_policy do |policy|
  if Rails.env.development?
    policy.script_src :self, :http, :unsafe_eval
  else
    policy.script_src :self, :https
  end
end
