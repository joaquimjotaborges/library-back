import Vue from 'vue'
import Router from 'vue-router'
import Signin from '@/components/Signin.vue'
import Signup from '@/components/Signup.vue'
import Books from '@/components/books/Books.vue'
import Authors from '@/components/authors/Authors.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/authors',
      name: 'Authors',
      component: Authors
    },
    {
      path: '/books',
      name: 'Books',
      component: Books
    },
    {
      path: '/',
      name: 'Signin',
      component: Signin
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    }
  ]
})
